/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.PersonRole;

import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.Organization.PersonOrganization;
import Business.Person.Person;
import Business.Person.PersonDirectory;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.ResourceHiringWorkRequest;

import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import userinterface.BankerRole.BankerWorkAreaJPanel;

/**
 *
 * @author NIKITA GUNDYAL
 */
public class ManageIndividualDetails extends javax.swing.JPanel {

    /**
     * Creates new form ManageIndividualDetails
     */
     private JPanel userProcessContainer;
     private Enterprise enterprise;
     private UserAccount userAccount;
     private PersonOrganization porganization;
     private Person p;
     private PersonDirectory dir;
    public ManageIndividualDetails(JPanel userProcessContainer,UserAccount userAccount,Enterprise enterprise,PersonOrganization porganization,Person p, PersonDirectory dir) {
        initComponents();
         this.userProcessContainer = userProcessContainer;
       this.enterprise = enterprise;
        this.userAccount = userAccount;
        this.porganization=porganization;
        this.p=p;
        this.dir=dir;
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        firstTxtField = new javax.swing.JTextField();
        lastTxtField = new javax.swing.JTextField();
        ageTxtField = new javax.swing.JTextField();
        ssnTxtField = new javax.swing.JTextField();
        phnTxtField = new javax.swing.JTextField();
        addBtn = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        domainTxtField = new javax.swing.JTextField();
        backBtn = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        DemoLabel = new javax.swing.JLabel();

        setBackground(new java.awt.Color(247, 250, 253));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel1.setText("First Name:");

        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel2.setText("Last Name:");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel3.setText("Age:");

        jLabel5.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel5.setText("SSN Number:");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel6.setText("Phone Number:");

        firstTxtField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                firstTxtFieldActionPerformed(evt);
            }
        });

        addBtn.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        addBtn.setText("Add");
        addBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBtnActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel8.setText("Domain:");

        domainTxtField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                domainTxtFieldActionPerformed(evt);
            }
        });

        backBtn.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        backBtn.setText("Back");
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(55, 103, 149));
        jPanel2.setForeground(new java.awt.Color(255, 255, 255));

        DemoLabel.setFont(new java.awt.Font("Century Gothic", 1, 33)); // NOI18N
        DemoLabel.setForeground(new java.awt.Color(255, 255, 255));
        DemoLabel.setText("INDIVIDUAL DETAILS");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(DemoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(444, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(DemoLabel)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel3)
                                .addComponent(jLabel2)
                                .addComponent(jLabel8)))
                        .addGap(40, 40, 40)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lastTxtField)
                            .addComponent(ageTxtField)
                            .addComponent(firstTxtField, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
                            .addComponent(domainTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(109, 109, 109)
                        .addComponent(backBtn)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 173, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(53, 53, 53)
                        .addComponent(ssnTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(addBtn)
                            .addComponent(phnTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(112, 112, 112))
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {ageTxtField, firstTxtField, lastTxtField, phnTxtField, ssnTxtField});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 188, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(firstTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5)
                    .addComponent(lastTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ssnTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(44, 44, 44)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel6)
                    .addComponent(ageTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(phnTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(domainTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(51, 51, 51)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addBtn)
                    .addComponent(backBtn))
                .addGap(71, 71, 71))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {ageTxtField, firstTxtField, lastTxtField, phnTxtField, ssnTxtField});

    }// </editor-fold>//GEN-END:initComponents

    private void firstTxtFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_firstTxtFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_firstTxtFieldActionPerformed

    private void addBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addBtnActionPerformed
        // TODO add your handling code here:
      try{
          if((firstTxtField.getText().equalsIgnoreCase("")) || (lastTxtField.getText().equalsIgnoreCase("")) ||  (domainTxtField.getText().equalsIgnoreCase("")) || (ageTxtField.getText().equalsIgnoreCase("")) || (ssnTxtField.getText().equalsIgnoreCase("")) ||(phnTxtField.getText().equalsIgnoreCase("")) ){
              
             JOptionPane.showMessageDialog(null, " Field cannot be empty ");
            }
          else if(Integer.parseInt(ageTxtField.getText()) < 0 || Integer.parseInt(ssnTxtField.getText()) <0 || Integer.parseInt(phnTxtField.getText()) < 0 ){
              
              JOptionPane.showMessageDialog(null, " These fields cannot have negative values ");
          }
               else
          {
               for(WorkRequest request : porganization.getWorkQueue().getWorkRequestList()){
               if(request instanceof ResourceHiringWorkRequest){
                dir=((ResourceHiringWorkRequest)request).getPersonDir();
               p= dir.createPerson();
               p.setFirstName(firstTxtField.getText());
                p.setLastName(lastTxtField.getText());
                p.setAge(Integer.parseInt(ageTxtField.getText()));
                p.setSsnNum(Integer.parseInt(ssnTxtField.getText()));
                p.setPhoneNum(Integer.parseInt(phnTxtField.getText()));
                p.setDomain(domainTxtField.getText());

                JOptionPane.showMessageDialog(null, " Person Added Successfully ");

               }
               
            
        }
        
          }
      }
      catch(Exception e){
          
          JOptionPane.showMessageDialog(null, " Enter Proper Data");
      }
    }//GEN-LAST:event_addBtnActionPerformed

    private void domainTxtFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_domainTxtFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_domainTxtFieldActionPerformed

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        // TODO add your handling code here:
            userProcessContainer.remove(this);
       Component[] componentArray = userProcessContainer.getComponents();
       Component component = componentArray[componentArray.length - 1];
       PersonWorkAreaJPanel dwjp = (PersonWorkAreaJPanel) component;
       dwjp.populateTable();
       CardLayout layout = (CardLayout) userProcessContainer.getLayout();
       layout.previous(userProcessContainer);
    }//GEN-LAST:event_backBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel DemoLabel;
    private javax.swing.JButton addBtn;
    private javax.swing.JTextField ageTxtField;
    private javax.swing.JButton backBtn;
    private javax.swing.JTextField domainTxtField;
    private javax.swing.JTextField firstTxtField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField lastTxtField;
    private javax.swing.JTextField phnTxtField;
    private javax.swing.JTextField ssnTxtField;
    // End of variables declaration//GEN-END:variables
}
