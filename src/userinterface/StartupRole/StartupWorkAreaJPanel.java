/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.StartupRole;

import Business.Enterprise.Enterprise;
import Business.Enterprise.FinanceEnterprise;
import Business.Enterprise.ResourceEnterprise;
import Business.Logic.EcoSystem;
import Business.Network.Network;
import Business.Organization.BankingOrganization;
import Business.Organization.LegalSupport;
import Business.Organization.Organization;
import static Business.Organization.Organization.Type.ResourceHiringOrganization;
import Business.Organization.PersonOrganization;
import Business.Organization.ResourceHiringOrganization;
import Business.Organization.StartupOrganization;
import Business.Person.Person;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.BankingFundingWorkRequest;
import Business.WorkQueue.InvestorFundingWorkRequest;
import Business.WorkQueue.LegalDocWorkRequest;
import Business.WorkQueue.ResourceHiringWorkRequest;
import Business.WorkQueue.WorkRequest;
import Bussiness.Mail.Mail;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.CardLayout;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Home
 */
public class StartupWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form StartupWorkArea
     */
    private JPanel userProcessContainer;
    private StartupOrganization organization;
    private Enterprise enterprise;
    private UserAccount userAccount;
    private EcoSystem business;

    public StartupWorkAreaJPanel(JPanel userProcessContainer, UserAccount userAccount, StartupOrganization organization, Enterprise enterprise, EcoSystem business) {
        initComponents();

        this.userProcessContainer = userProcessContainer;
        this.organization = organization;
        this.enterprise = enterprise;
        this.userAccount = userAccount;
        this.business = business;
        valueLabel.setText(enterprise.getName());
        populateRequestTable();
        populateBankerTable();
    }

    public void populateRequestTable() {
        DefaultTableModel model = (DefaultTableModel) workRequestJTable.getModel();

        model.setRowCount(0);
        for (WorkRequest request : userAccount.getWorkQueue().getWorkRequestList()) {
            if (request instanceof InvestorFundingWorkRequest) 
            {
                Object[] row = new Object[8];
                row[0] = request.getMessage();
                row[1] = request.getReceiver();
                row[2] = request.getSender();
                row[3] = request.getStatus();
                row[4] = request.getDomain();
                row[5] = request.getFunding();
                row[6] = request.getFundedAmount();
                String result = ((InvestorFundingWorkRequest) request).getTestResult();
                row[7] = result == null ? "Waiting" : result;
                model.addRow(row);
            }

        }
    }

    public void populateBankerTable() {
        DefaultTableModel model = (DefaultTableModel) BankingTbl.getModel();

        model.setRowCount(0);

        for (Network n : business.getNetworkList()) {
            for (Enterprise e : n.getEnterpriseDirectory().getEnterpriseList()) {
                if (e instanceof FinanceEnterprise) {
                    for (Organization o : e.getOrganizationDirectory().getOrganizationList()) {
                        if (o instanceof BankingOrganization) {
                            for (WorkRequest wr : o.getWorkQueue().getWorkRequestList()) {
                                if (wr instanceof BankingFundingWorkRequest) {
                                    Object[] row = new Object[8];
                                    row[0] = wr.getMessage();
                                    row[1] = wr.getReceiver();
                                    row[2] =  wr.getBankName();
                                    row[3] =wr.getBankBranch();
                                    row[4]= wr.getBankAccountNum();
                                    row[5]=wr.getBankRoutingNum();
                                    row[6]=wr.getProcessedRequest();
                                    String r = ((BankingFundingWorkRequest) wr).getTestResult();
                                    row[7] = r == null ? "Waiting" : r;
                                    model.addRow(row);

                                }

                            }
                        }
                    }

                }
            }
        }

    }
    public void populateLegalTable() {
        DefaultTableModel model = (DefaultTableModel) legalTable.getModel();

        model.setRowCount(0);

        for (Network n : business.getNetworkList()) {
            for (Enterprise e : n.getEnterpriseDirectory().getEnterpriseList()) {
                if (e instanceof FinanceEnterprise) {
                    for (Organization o : e.getOrganizationDirectory().getOrganizationList()) {
                        if (o instanceof LegalSupport ) {
                            for (WorkRequest wr : o.getWorkQueue().getWorkRequestList()) {
                                if (wr instanceof LegalDocWorkRequest) {
                                    Object[] row = new Object[5];
                                    row[0] = wr.getMessage();
                                    row[1] = wr.getLicenseNum();
                                    row[2] = wr.getFeinNUM();
                                    row[3] = wr.getStatus();
                                     String r = ((LegalDocWorkRequest) wr).getTestResult();
                                    row[4] =  r == null ? "Waiting" : r;
                                     model.addRow(row);

                                }

                            }
                        }
                    }

                }
            }
        }

    }
    
    
    public void populateResourceTable(){
        
        DefaultTableModel model = (DefaultTableModel) resourceTable.getModel();

        model.setRowCount(0);

        for (Network n : business.getNetworkList()) {
            for (Enterprise e : n.getEnterpriseDirectory().getEnterpriseList()) {
                if (e instanceof ResourceEnterprise) {
                    for (Organization o : e.getOrganizationDirectory().getOrganizationList()) {
                        if (o instanceof PersonOrganization) {
                            for (WorkRequest wr : o.getWorkQueue().getWorkRequestList()) {
                                if (wr instanceof ResourceHiringWorkRequest) {
                                    for(Person p1 :((ResourceHiringWorkRequest) wr).getPersonDir().getPersonDir()){
                                          Object[] row = new Object[7];
                                    row[0] = p1.getFirstName();
                                    row[1] = p1.getLastName();
                                    row[2] = p1.getAge();
                                    row[3] = p1.getSsnNum();
                                    row[4]= p1.getPhoneNum();
                                    row[5]= wr.getSender();
                                    String r = ((ResourceHiringWorkRequest) wr).getTestResult();
                                    row[6]= r == null ? "Waiting" : r;
                                    model.addRow(row);

                                    }
                                  
                                }

                            }
                        }
                    }

                }
            }
        }

        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        workRequestJTable = new javax.swing.JTable();
        requestTestJButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        valueLabel = new javax.swing.JLabel();
        refreshTestJButton = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        DemoLabel = new javax.swing.JLabel();
        ReqBankAcc = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        BankingTbl = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        hireBtn = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        resourceTable = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        legalTable = new javax.swing.JTable();
        legalBtn = new javax.swing.JButton();

        setBackground(new java.awt.Color(247, 250, 253));

        workRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Startup Idea", "Receiver", "Sender", "Status", "Domain", "Funding Amount", "Funded Amount", "Result"
            }
        ));
        jScrollPane1.setViewportView(workRequestJTable);

        requestTestJButton.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        requestTestJButton.setText("Request Funding");
        requestTestJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                requestTestJButtonActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel1.setText("ENTERPRISE");

        valueLabel.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        valueLabel.setText("enterprise");

        refreshTestJButton.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        refreshTestJButton.setText("Refresh");
        refreshTestJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshTestJButtonActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(55, 103, 149));
        jPanel2.setForeground(new java.awt.Color(255, 255, 255));

        DemoLabel.setFont(new java.awt.Font("Century Gothic", 1, 33)); // NOI18N
        DemoLabel.setForeground(new java.awt.Color(255, 255, 255));
        DemoLabel.setText("START-UP WORK AREA");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(DemoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 427, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(DemoLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        ReqBankAcc.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        ReqBankAcc.setText("Request Bank Acc");
        ReqBankAcc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReqBankAccActionPerformed(evt);
            }
        });

        BankingTbl.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        BankingTbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Request", "Receiver", "Bank Name", "Bank Branch", "Bank Account Number", "Bank Routing Number", "Request Message", "Result"
            }
        ));
        jScrollPane2.setViewportView(BankingTbl);

        jLabel2.setText("Investor Request Data");

        jLabel3.setText("Banker Request Data:");

        hireBtn.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        hireBtn.setText("Hire a resource");
        hireBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hireBtnActionPerformed(evt);
            }
        });

        jLabel4.setText("Resource Data:");

        resourceTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "First Name", "Last Name", "Age", "SSN Number", "Phone Number", "Sender", "Result"
            }
        ));
        jScrollPane3.setViewportView(resourceTable);

        jButton1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jButton1.setText("Export to PDF");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel5.setText("Legal Data:");

        legalTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Request", "License Number", "FEIN Number", "Status", "Result"
            }
        ));
        jScrollPane4.setViewportView(legalTable);

        legalBtn.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        legalBtn.setText("Request for Legal Documents");
        legalBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                legalBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addContainerGap(923, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(30, 30, 30)
                        .addComponent(valueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(refreshTestJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(149, 149, 149))))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane4)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(requestTestJButton)
                                        .addGap(26, 26, 26)
                                        .addComponent(ReqBankAcc, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(47, 47, 47)
                                        .addComponent(hireBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(48, 48, 48)
                                        .addComponent(legalBtn)))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(84, 84, 84))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(valueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(refreshTestJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(45, 45, 45)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(jLabel5)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(requestTestJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ReqBankAcc, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(hireBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(legalBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(474, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void requestTestJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_requestTestJButtonActionPerformed
        // TODO add your handling code here:

        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add("RequestLabTestJPanel", new RequestStartup(userProcessContainer, userAccount, enterprise));
        layout.next(userProcessContainer);
        JOptionPane.showMessageDialog(null, " Request For Funding Successfully ");
    }//GEN-LAST:event_requestTestJButtonActionPerformed

    private void refreshTestJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshTestJButtonActionPerformed
        // TODO add your handling code here:

        populateRequestTable();
        populateBankerTable();
        populateResourceTable();
        populateLegalTable();
    }//GEN-LAST:event_refreshTestJButtonActionPerformed

    private void ReqBankAccActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReqBankAccActionPerformed
        // TODO add your handling code here:
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        //userProcessContainer.add("RequestLabTestJPanel", new userinterface.StartupRole.RequestBankAccount(userProcessContainer, userAccount, enterprise));
        userProcessContainer.add("RequestLabTestJPanel", new userinterface.StartupRole.RequestBankAccount(userProcessContainer, userAccount, business,enterprise));
        layout.next(userProcessContainer);
        JOptionPane.showMessageDialog(null, " Request For Creation of Bank Details Submitted ");
    }//GEN-LAST:event_ReqBankAccActionPerformed

    private void hireBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hireBtnActionPerformed
        // TODO add your handling code here:
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        //userProcessContainer.add("RequestLabTestJPanel", new userinterface.StartupRole.RequestBankAccount(userProcessContainer, userAccount, enterprise));
        userProcessContainer.add("RequestLabTestJPanel", new userinterface.StartupRole.RequestHireResource(userProcessContainer, userAccount, business));
        layout.next(userProcessContainer);
        JOptionPane.showMessageDialog(null, " Request For Resources Submiited ");
    }//GEN-LAST:event_hireBtnActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

           int selectedRow = BankingTbl.getSelectedRow();
           WorkRequest request = null;
           if(selectedRow<0)
           {
                JOptionPane.showMessageDialog(null, "Please select a row from the table first", "Warning", JOptionPane.WARNING_MESSAGE);
                return;
           }
          if(selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row from the table first", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
            }
          if(selectedRow >= 0)
          {
              request = (WorkRequest) BankingTbl.getValueAt(selectedRow, 0);
               try
         {
             // TODO add your handling code here:
             try
             {String pdfFilePath = "C:\\Users\\NIKITA GUNDYAL\\Desktop\\aed\\BankReport.pdf";
             OutputStream fos = new FileOutputStream(new File(pdfFilePath));
             Document document = new Document();
             PdfWriter pdfWriter = PdfWriter.getInstance(document, fos);
             
             String userPassword = "1234";
             String ownerPassword = "1234";
             
             pdfWriter.setEncryption(userPassword.getBytes(),
                     ownerPassword.getBytes(), PdfWriter.ALLOW_PRINTING,
                     PdfWriter.ENCRYPTION_AES_128);
             
             document.open();
             document.add(new Paragraph("This is Password protected PDF file" +request.getBankAccountNum()+"\n"+
                     request.getBankName()+"\n"+request.getBankBranch()+"\n"+request.getBankRoutingNum()));
             
             document.close();
             fos.close();
             
             System.out.println("PDF created in >> " + pdfFilePath);
             }
             catch (Throwable e)
             {
                 e.printStackTrace();
             }
             
             String[] to = {"ruchi.niwalkar01@gmail.com"};
             String message= " The password is 1234 ";
             
             if (Mail.sendMail("startupgeekyisotopes", "geekyisotopes123", message, to))
             {
                 System.out.println("sent");
             }
             else
             {
                 System.out.println("error");
             }
             
             
         }
          catch (MessagingException ex)
       {
           Logger.getLogger(StartupWorkAreaJPanel.class.getName()).log(Level.SEVERE, null, ex);
       }
       JOptionPane.showMessageDialog(this, "Email Sent Successfully!");
         JOptionPane.showMessageDialog(this, "Email Sent Successfully!");
         
              
              
              
          }
      
       
    }//GEN-LAST:event_jButton1ActionPerformed

    private void legalBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_legalBtnActionPerformed
        // TODO add your handling code here:
         CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        //userProcessContainer.add("RequestLabTestJPanel", new userinterface.StartupRole.RequestBankAccount(userProcessContainer, userAccount, enterprise));
        userProcessContainer.add("RequestLabTestJPanel", new userinterface.StartupRole.RequestLegalization(userProcessContainer, userAccount, business));
        layout.next(userProcessContainer);
        JOptionPane.showMessageDialog(null, " Request For Legal Documents Submitted ");
        
    }//GEN-LAST:event_legalBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JTable BankingTbl;
    private javax.swing.JLabel DemoLabel;
    private javax.swing.JButton ReqBankAcc;
    private javax.swing.JButton hireBtn;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JButton legalBtn;
    private javax.swing.JTable legalTable;
    private javax.swing.JButton refreshTestJButton;
    private javax.swing.JButton requestTestJButton;
    private javax.swing.JTable resourceTable;
    private javax.swing.JLabel valueLabel;
    private javax.swing.JTable workRequestJTable;
    // End of variables declaration//GEN-END:variables
}
