/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Enterprise.Enterprise;
import Business.Logic.EcoSystem;
import Business.Organization.InvestorsOrganization;
import Business.Organization.Organization;
import Business.Organization.StartupOrganization;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;
import userInterface.InvestorRole.InvestorWorkAreaJPanel;
import userInterface.StartupRole.StartupWorkAreaJPanel;

/**
 *
 * @author nikita
 */
public class InvestorRole extends Role{
    
    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        
        return new InvestorWorkAreaJPanel(userProcessContainer,account,organization, business);
    }
    
}
