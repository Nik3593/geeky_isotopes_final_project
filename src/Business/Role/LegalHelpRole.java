/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Enterprise.Enterprise;
import Business.Logic.EcoSystem;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;
import serinterface.LegalHelpRole.LegalHelpWorkAreaJPanel;
import userinterface.BankerRole.BankerWorkAreaJPanel;

/**
 *
 * @author Home
 */
public class LegalHelpRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new LegalHelpWorkAreaJPanel(userProcessContainer,account, organization, business); //To change body of generated methods, choose Tools | Templates.
    }
    
}
