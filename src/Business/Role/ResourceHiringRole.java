/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;


import Business.Enterprise.Enterprise;
import Business.Logic.EcoSystem;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;
import userInterface.InvestorRole.InvestorWorkAreaJPanel;
import userinterface.BankerRole.BankerWorkAreaJPanel;
import userinterface.ResourceHiringRole.ResourceHiringWorkareaJPanel;

/**
 *
 * @author Home
 */
public  class ResourceHiringRole extends Role {
    

    
    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
         return new ResourceHiringWorkareaJPanel(userProcessContainer,account,organization, business);    }

  
}
