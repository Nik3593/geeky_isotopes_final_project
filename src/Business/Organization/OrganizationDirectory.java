/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import java.util.ArrayList;

/**
 *
 * @author nikita
 */
public class OrganizationDirectory {
    
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Organization.Type type){
        Organization organization = null;
        if (type.getValue().equals(Organization.Type.Startup.getValue())){
            organization = new StartupOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Organization.Type.Investors.getValue())){
            organization = new InvestorsOrganization();
            organizationList.add(organization);
        }
         else if (type.getValue().equals(Organization.Type.Banking.getValue())){
            organization = new BankingOrganization();
            organizationList.add(organization);
        }
         else if (type.getValue().equals(Organization.Type.Person.getValue())){
            organization = new PersonOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Organization.Type.LegalSupport.getValue())){
            organization = new LegalSupport();
            organizationList.add(organization);
        }
        
        return organization;
    }
    
}
