/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.BankerRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Home
 */
public class ResourceHiringOrganization extends Organization{
    
     public ResourceHiringOrganization()
     {
        
        super(Organization.Type.ResourceHiringOrganization.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new BankerRole());
        return roles;
    }
   
    
}
