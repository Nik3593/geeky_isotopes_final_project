/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Role;
import Business.Role.StartupRole;
import java.util.ArrayList;

/**
 *
 * @author nikita
 */
public class StartupOrganization extends Organization {
    
    public StartupOrganization(){
        
        super(Organization.Type.Startup.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new StartupRole());
        return roles;
    }
    
}
