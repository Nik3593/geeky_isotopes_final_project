/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Person;

import java.util.ArrayList;

/**
 *
 * @author NIKITA GUNDYAL
 */
public class PersonDirectory {
    
    private ArrayList<Person> personDir;
    
    public PersonDirectory(){
        personDir= new ArrayList<Person>();
     }

    public ArrayList<Person> getPersonDir() {
        return personDir;
    }

    public void setPersonDir(ArrayList<Person> personDir) {
        this.personDir = personDir;
    }
    
    public Person createPerson(){
        
        Person person = new Person();
        personDir.add(person);
        return person;
        
    }
    
    
    
}
