/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Person;

/**
 *
 * @author NIKITA GUNDYAL
 */
public class Person {
    
    private String firstName;
    private String lastName;
    private String emailId;
    private int age;
    private int ssnNum;
    private int phoneNum;
    private String domain;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }
    
    
    public Person(){
        
        this.firstName=firstName;
        this.lastName=lastName;
        this.age=age;
        this.emailId=emailId;
        this.phoneNum=phoneNum;
        this.ssnNum=ssnNum;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSsnNum() {
        return ssnNum;
    }

    public void setSsnNum(int ssnNum) {
        this.ssnNum = ssnNum;
    }

    public int getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(int phoneNum) {
        this.phoneNum = phoneNum;
    }
    
    
    
    
}
