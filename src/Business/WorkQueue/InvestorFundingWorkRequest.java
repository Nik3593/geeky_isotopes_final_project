/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author nikita
 */
public class InvestorFundingWorkRequest extends WorkRequest{
    
     private String fundingResult;
     private String testResult;

    public String getFundingResult() {
        return fundingResult;
    }

    public void setFundingResult(String fundingResult) {
        this.fundingResult = fundingResult;
    }

     
    public String getTestResult() {
        return fundingResult;
    }

    public void setTestResult(String testResult) {
        this.fundingResult = testResult;
    }
    
}
