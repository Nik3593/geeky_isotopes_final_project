/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Person.PersonDirectory;

/**
 *
 * @author NIKITA GUNDYAL
 */
public class ResourceHiringWorkRequest  extends WorkRequest{
    
      private String fundingResult;
      private String domainNeeded;
      private PersonDirectory personDir;
      private String testResult;
      
      public ResourceHiringWorkRequest(){
          
         this.personDir = new PersonDirectory();
      }

    public PersonDirectory getPersonDir() {
        return personDir;
    }

    public void setPersonDir(PersonDirectory personDir) {
        this.personDir = personDir;
    }
      
      

    public String getFundingResult() {
        return fundingResult;
    }

    public void setFundingResult(String fundingResult) {
        this.fundingResult = fundingResult;
    }

    public String getDomainNeeded() {
        return domainNeeded;
    }

    public void setDomainNeeded(String domainNeeded) {
        this.domainNeeded = domainNeeded;
    }
      

    public String getTestResult() {
        return fundingResult;
    }

    public void setTestResult(String testResult) {
        this.fundingResult = testResult;
    }
    
        
    
}
