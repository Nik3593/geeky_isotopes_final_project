/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.UserAccount.UserAccount;
import java.util.Date;

/**
 *
 * @author nikita
 */
public class WorkRequest {
    
    private String message;
    private String investmentInterest;
    private String funding;
    private String fundedAmount;
    private String domain;
    private String bankName;
    private String bankBranch;
    private String bankAccountNum;
    private String bankRoutingNum;
    private UserAccount sender;
    private UserAccount receiver;
    private String status;
    private Date requestDate;
    private Date resolveDate;
    private String processedRequest;
    private String response;
    private String email;
    private String licenseNum;
    private String feinNUM;

    public String getLicenseNum() {
        return licenseNum;
    }

    public void setLicenseNum(String licenseNum) {
        this.licenseNum = licenseNum;
    }

    public String getFeinNUM() {
        return feinNUM;
    }

    public void setFeinNUM(String feinNUM) {
        this.feinNUM = feinNUM;
    }
    

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
    

    public String getProcessedRequest() {
        return processedRequest;
    }

    public void setProcessedRequest(String processedRequest) {
        this.processedRequest = processedRequest;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getBankAccountNum() {
        return bankAccountNum;
    }

    public void setBankAccountNum(String bankAccountNum) {
        this.bankAccountNum = bankAccountNum;
    }

    public String getBankRoutingNum() {
        return bankRoutingNum;
    }

    public void setBankRoutingNum(String bankRoutingNum) {
        this.bankRoutingNum = bankRoutingNum;
    }
    

    public String getFundedAmount() {
        return fundedAmount;
    }

    public void setFundedAmount(String fundedAmount) {
        this.fundedAmount = fundedAmount;
    }
    
    public WorkRequest(){
        requestDate = new Date();
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    
    public UserAccount getSender() {
        return sender;
    }

    public void setSender(UserAccount sender) {
        this.sender = sender;
    }

    public UserAccount getReceiver() {
        return receiver;
    }

    public void setReceiver(UserAccount receiver) {
        this.receiver = receiver;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getResolveDate() {
        return resolveDate;
    }

    public void setResolveDate(Date resolveDate) {
        this.resolveDate = resolveDate;
    }

    public String getInvestmentInterest() {
        return investmentInterest;
    }

    public void setInvestmentInterest(String investmentInterest) {
        this.investmentInterest = investmentInterest;
    }

    public String getFunding() {
        return funding;
    }

    public void setFunding(String funding) {
        this.funding = funding;
    }
    
    
    
    
}
